<?php
    include "db.php";
    include "query.php"
?>
<html>
    <head>
        <title>Books</title>
    </head>
    <body>
        <p>
            <?php
                $db = new DB('localhost', 'intro', 'root', '');
                $dbc = $db->connect();
                $query = new Query($dbc);
                $q ="SELECT b.id, b.author, b.title_field, b.user_id, u.name
                    FROM books b, users u
                    WHERE b.user_id=u.id";
                $result=$query->query($q);
                echo '<br>'; 
                if($result->num_rows>0){
                    echo '<table>';
                    echo '<tr>
                            <th>Book Id</th>
                            <th>Book Author</th>
                            <th>Book Title</th>
                            <th>User Name</th>
                        </tr>';
                    while($row=$result->fetch_assoc()){
                        echo '<tr>';
                            echo '<td>'.$row['id'].'</td>
                                <td>'.$row['author'].'</td>
                                <td>'.$row['title_field'].'</td>
                                <td>'.$row['name'].'</td>';
                        echo '</tr>';
                    }
                    echo '</table>';
                }else{
                    echo "sorry no results";
                }
            ?>
        </p>
    </body>
</html>